package main

import (
	"net/http"
	"encoding/json"
	"bytes"
	"github.com/go-redis/redis"
	"github.com/magiconair/properties"
	"github.com/buger/jsonparser"
	"io/ioutil"
)

func initRedis() *redis.Client{
	p := properties.MustLoadFile("/etc/service.conf", properties.UTF8)
	address := p.GetString("redis.name","localhost")+":"+p.GetString("redis.port","6379")
	client := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: p.GetString("redis.password",""),
		DB:       p.GetInt("redis.db",0),
	})
	return client
}

func main() {
	http.HandleFunc("/digiroin/service/postpaid/inquiry", inquiry)
	http.HandleFunc("/digiroin/service/postpaid/payment", payment)
	http.ListenAndServe(":7060", nil)
}

type Inquiry struct {
	CustomerId string
	Cashtag string
	Type string
}

type Payment struct {
	CustomerId string
	Cashtag string
	Reference string
	Nominal string
	Type string
}

func inquiry(w http.ResponseWriter, r *http.Request) {
	response := Inquiry{}
	client := &http.Client{}
	redis := initRedis()
	err := json.NewDecoder(r.Body).Decode(&response)
	result :=""
	if(err!=nil){
		w.WriteHeader(http.StatusBadRequest)
		result = `{"Error":"`+err.Error()+`"}`
	}else{
		redisValue := redis.HGet("service","postpaidinquiry$"+response.Cashtag).Val()
		product := redis.Get(response.Cashtag+":"+response.Type).Val()
		if(redisValue=="" || product==""){
			w.WriteHeader(http.StatusBadRequest)
			result = `{"Error":"cashtag or product not found"}`
		}else{
			bodyReq := `{"ProductCode":"`+product+`","Number1":"`+response.CustomerId+`","Number3":"","Number2":"","Misc":""}`
			var jsonStr = []byte(bodyReq)
			req, err := http.NewRequest("POST", redisValue, bytes.NewBuffer(jsonStr))
			resp, err := client.Do(req)
			if(err==nil){
				res,err := ioutil.ReadAll(resp.Body)
				if err==nil{
					if resp.StatusCode==200{
						fix,_ := jsonparser.ParseString(res)
						result = fix
					}else{
						error,_ := jsonparser.ParseString(res)
						result =error
						w.WriteHeader(http.StatusInternalServerError)
					}
				}else{
					result = `{"Error":"`+err.Error()+`"}`
					w.WriteHeader(http.StatusInternalServerError)
				}
			}else{
				result = `{"Error":"`+err.Error()+`"}`
				w.WriteHeader(http.StatusInternalServerError)
			}

		}
	}
	w.Write([]byte(result))
}

func payment(w http.ResponseWriter, r *http.Request) {
	response := Payment{}
	client := &http.Client{}
	redis := initRedis()
	err := json.NewDecoder(r.Body).Decode(&response)
	result :=""
	if(err!=nil){
		w.WriteHeader(http.StatusBadRequest)
		result = `{"Error":"cashtag or product not found"}`
	}else{
		redisValue := redis.HGet("service","postpaidpayment$"+response.Cashtag).Val()
		product := redis.Get(response.Cashtag+":"+response.Type).Val()
		if(redisValue=="" || product==""){
			w.WriteHeader(http.StatusBadRequest)
			result = `{"Error":"cashtag not found"}`
		}else{
			bodyReq := `{"ProductCode":"`+product+`","Number1":"`+response.CustomerId+`","Number3":"","Number2":"","Nominal":"`+response.Nominal+`","RefNumber":"`+response.Reference+`"}`
			var jsonStr = []byte(bodyReq)
			req, err := http.NewRequest("POST", redisValue, bytes.NewBuffer(jsonStr))
			resp, err := client.Do(req)
			if(err==nil){
				res,err := ioutil.ReadAll(resp.Body)
				if err==nil{
					if resp.StatusCode==200{
						fix,_ := jsonparser.ParseString(res)
						result = fix
					}else{
						error,_ := jsonparser.ParseString(res)
						result =error
						w.WriteHeader(http.StatusInternalServerError)
					}
				}else{
					result = `{"Error":"`+err.Error()+`"}`
					w.WriteHeader(http.StatusInternalServerError)
				}
			}else{
				result = `{"Error":"`+err.Error()+`"}`
				w.WriteHeader(http.StatusInternalServerError)
			}
		}
	}
	w.Write([]byte(result))
}